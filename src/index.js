import React from 'react';
import ReactDOM from 'react-dom';

// routes
import { BrowserRouter, Switch, Route } from 'react-router-dom';

// Redux
import { Provider } from 'react-redux';
import Store from './store';

import './assets/styles/main.scss';

// components
import Home from './containers/Home';
import Details from './containers/Details';
import Search from './containers/Search';

// service worker
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <Provider store={Store}>
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/details/:id" component={Details} />
          <Route path="/items/:query" component={Search} />
          <Route component={Home} />
        </Switch>
      </BrowserRouter>
    </Provider>,
    document.getElementById('root')
  );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
