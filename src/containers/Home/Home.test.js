import React, { Component } from 'react';
import './Home.scss';

// components
import Header from "./../../components/header";

class Home extends Component {
  constructor(props){
    super(props);
    this.updateResults = this.updateResults.bind(this);
  }

  render() {

    return (
      <div className="home">
        <Header
          searchQuery={this.updateResults}
        />
      </div>
    );
  }
}

export default Home;