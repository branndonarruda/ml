import React, { Component } from 'react';

// components
import Header from "./../../components/header";

class Home extends Component {
  constructor(props){
    super(props);

    this.updateResults = this.updateResults.bind(this);
  }

  updateResults(query) {
    this.props.history.push(`/items/${query}`);
  }

  render() {

    return (
      <div className="home">
        <Header
          searchQuery={this.updateResults}
        />
      </div>
    );
  }
}

export default Home;