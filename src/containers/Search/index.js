import React, { Component } from 'react';
import { connect } from 'react-redux';

import './Search.scss';

// thunks
import SearchThunk from '../../store/thunks/SearchThunk';

// components
import Header from "./../../components/header";
import Breadcrumb from "./../../components/breadcrumb";
import Boxwhite from "./../../components/boxwhite";
import Product from "./../../components/product";

class Search extends Component {
  constructor(props){
    super(props);
    this.updateResults = this.updateResults.bind(this);
  }

  updateResults(query) {
    this.props.search(query);
    this.props.history.push(`/items/${query}`);
  }

  /**
   * before load get results from URL param
   */
  componentDidMount(query) {
    this.props.search(this.props.match.params.query);
  }

  render() {
    const {results} = this.props;

    return (
      <div className="search">
        <Header
          searchQuery={this.updateResults}
          data={this.props.match.params.query}
        />

        <div className="container">
          <Breadcrumb item={this.props.match.params.query} />

          <Boxwhite>
            <div className="search__products">
              {results.length ? (
                results.map((item, index) =>
                  <Product
                    key={item.id}
                    price={item.price}
                    address={item.address}
                    title={item.title}
                    thumbnail={item.thumbnail}
                    id={item.id}
                  />
                )
              ) : (
                <div>Pesquisando..</div>
              )}
            </div>
          </Boxwhite>
        </div>

      </div>
    );
  }
}

// map state from store to read inside this component
const mapStateToProps = state => ({
  results: state.search.results
});

// map dispatch(functions) from store to call inside this component
const mapDispatchToProps = dispatch => ({
  search: (data) => {
    dispatch(SearchThunk.search(data));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);