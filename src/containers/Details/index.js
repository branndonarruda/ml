import React, { Component } from 'react';
import CurrencyFormat from 'react-currency-format';
import { connect } from 'react-redux';

import './Details.scss';

// thunks
import DetailsThunk from '../../store/thunks/DetailsThunk';

// components
import Header from "./../../components/header";
import Boxwhite from "./../../components/boxwhite";
import { Button } from '../../components/button';

class Details extends Component {
  constructor(props){
    super(props);
    this.updateResults = this.updateResults.bind(this);
  }

  componentDidMount() {
      this.props.getDetails(this.props.match.params.id);
      this.props.getDescription(this.props.match.params.id);
  }

  /**
   * Redirect user to search page and show results
   * @param {string} query 
   */
  updateResults(query) {
    this.props.history.push(`/items/${query}`);
  }

  render() {
    const pictures = this.props.infos.pictures;
    const mainPicture = "";

    if (pictures !== undefined) {
      if (pictures.length > 1) {
        this.mainPicture = <img src={pictures[0].url} alt="" title="" />
      }
    } else {
      this.mainPicture = "";
    }

    return (
      <div className="details">
        <Header
          searchQuery={this.updateResults}
        />

          <div className="container container-internal">

            {!this.props.description &&
              <Boxwhite>
                <div className="details__container">
                  <p>Informação não encontrada.</p>
                </div>
              </Boxwhite>
            }
            {this.props.description &&
              <Boxwhite>
                  <div className="details__container">
                    <div className="details__container__main main">
                      <div className="main__img">
                        {this.mainPicture}
                      </div>
                      <h2 className="main__title">
                        Descrição
                      </h2>
                      <p className="main__description">
                        {this.props.description}
                      </p>
                    </div>

                    <div className="details__container__product-info product-info">

                      <p className="product-info__condition">{this.props.infos.condition} - 234 vendidos</p>
                      <p className="product-info__title">
                        {this.props.infos.title}
                      </p>
                      <p className="product-info__price">
                        R$ <CurrencyFormat value={this.props.infos.price} displayType={'text'} format="#.###" />
                      </p>

                      <Button>Comprar</Button>
                    </div>
                  </div>
              </Boxwhite>
            }
          </div>
      </div>
    );
  }
}

// map state from store to read inside this component
const mapStateToProps = state => ({
  infos: state.details.product,
  description: state.details.description
});

// map dispatch(functions) from store to call inside this component
const mapDispatchToProps = dispatch => ({
  getDetails: (data) => {
    dispatch(DetailsThunk.getProductInfo(data));
  },
  getDescription: (data) => {
    dispatch(DetailsThunk.getDescription(data));
  }
});

Details.defaultProps = {
  info: {
    description: '',
    title: '',
    price: '',
    condition: '',
    pictures: []
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Details);