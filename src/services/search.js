import Axios from 'axios';

export const Search = {
    getProducts: (url) => {
        return Axios.get(url);
    }
}