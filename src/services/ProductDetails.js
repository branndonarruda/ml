import Axios from 'axios';

export const ProductDetails = {
    getInfo: (url) => {
        return Axios.get(url);
    }
}