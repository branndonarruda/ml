import React from 'react';
import ReactDOM from 'react-dom';

// Enzyme lib
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';

// component
import Boxwhite from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Boxwhite />, div);
  ReactDOM.unmountComponentAtNode(div);
});

// snapshot test
it('Snapshot - Boxwhite', () => {
    const component = mount(<Boxwhite />);
    expect(toJson(component)).toMatchSnapshot();
});
