import React from 'react';
import './Boxwhite.scss';


export const Boxwhite = (props) => {
    return (
        <div className="boxwhite">
            {props.children}
        </div>
    );
  };

export default Boxwhite;