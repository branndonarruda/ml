import React from 'react';
import ReactDOM from 'react-dom';

// Enzyme lib
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';

// component
import Header from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Header />, div);
  ReactDOM.unmountComponentAtNode(div);
});

// snapshot test
it('Snapshot - Header', () => {
    const component = mount(<Header />);
    expect(toJson(component)).toMatchSnapshot();
});
