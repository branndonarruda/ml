import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Header.scss';

// images
import logo from '././../../assets/images/Logo_ML.png';
import searchIcon from '././../../assets/images/ic_Search.png';

class Header extends Component {
    constructor(props){
        super(props);

        this.state = {value: ''};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount () {
        this.setState({
            value : this.props.data
        });
    }

    /**
     *  send field value to parent component
     * @param {any} event 
     */
    handleSubmit(event) {
        event.preventDefault();
        const query = this.refs.searchInput.value;
        this.props.searchQuery(query);
    }

    /**
     * update input field value
     */
    handleChange = (event) => {
        this.setState({
            value : event.target.value
        });
    }

    render() {

        return(
            <div className="header">
                <div className="header__container container">
                    <div role="img" aria-label="Logo Mercado Livre">
                        <Link to={'/'}>
                            <img
                                tabIndex="1"
                                src={logo}
                                alt="Mercado Livre"
                                title="Mercado Livre"
                            />
                        </Link>
                    </div>
                    <form className="search-bar" onSubmit={this.handleSubmit}>
                        <input
                            tabIndex="1"
                            ref="searchInput"
                            className="search-bar__field"
                            placeholder="Buscar produtos, marcas e muito mais…"
                            aria-label="Digite o que você quer encontrar"
                            autoCapitalize="off"
                            autoCorrect="off"
                            spellCheck="false"
                            autoComplete="off"
                            value={this.state.value || ''}
                            onChange={this.handleChange}
                        />
        
                        <button
                            tabIndex="1"
                            type="submit"
                            className="search-bar__btn"
                        >
                            <img
                                src={searchIcon}
                                alt="Icone de lupa"
                            />
                        </button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Header;