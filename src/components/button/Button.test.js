import React from 'react';
import ReactDOM from 'react-dom';

// Enzyme lib
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';

// component
import Button from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Button>Pesquisar</Button>, div);
  ReactDOM.unmountComponentAtNode(div);
});

// snapshot test
it('Snapshot - Button', () => {
    const component = mount(<Button>Pesquisar</Button>);
    expect(toJson(component)).toMatchSnapshot();
});
