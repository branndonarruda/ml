import React from 'react';
import { Link } from 'react-router-dom';
import CurrencyFormat from 'react-currency-format';
import './Product.scss';

const Product = ({ id, price, title, address, thumbnail }) => (
    <div className="product">
        <div className="product__image">
            <Link to={'/details/' + id}>
                <img
                    src={thumbnail}
                    alt={title}
                    title={title}
                    tabIndex="1"
                />
            </Link>
        </div>

        <div className="product__infos">
            <Link to={'/details/' + id} tabIndex="1">
                <h2 className="product__infos__price" title="Clique e saiba mais">R$ <CurrencyFormat value={price} displayType={'text'} format="#.###" /></h2>
            </Link>
            <p
                className="product__infos__description"
                tabIndex="1"
            >
                {title}
            </p>
        </div>

        <p className="product__location" tabIndex="1">
            {address.city_name}
        </p>
    </div>
);

export default Product;

Product.defaultProps = {
    price: '',
    title: '',
    id: ''
};