import React from 'react';
import PropTypes from 'prop-types';
import './Breadcrumb.scss';


const Breadcrumb = ({ item }) => (
    <div className="breadcrumb">
        <p className="breadcrumb__text">Você Pesquisou por: <strong>{item}</strong></p>
    </div>
);

export default Breadcrumb;

Breadcrumb.propTypes = {
  item: PropTypes.string
};

Breadcrumb.defaultProps = {
  item: ''
};