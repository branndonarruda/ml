import React from 'react';
import ReactDOM from 'react-dom';

// Enzyme lib
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';

const props = {
    item: 'iPod'
}

// component
import Breadcrumb from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Breadcrumb />, div);
  ReactDOM.unmountComponentAtNode(div);
});

// snapshot test
it('Snapshot - Breadcrumb', () => {
    const component = mount(<Breadcrumb {...props} />);
    expect(toJson(component)).toMatchSnapshot();
});
