import SearchAction from '../actions/SearchAction';
import { Search } from './../../services/search';

const SearchThunk = {
  search: query => {
    return async dispatch => {

      try {
        const response = await Search.getProducts(`https://api.mercadolibre.com/sites/MLA/search?q=${query}`);
        dispatch(SearchAction.search(response.data.results));
      }

      catch (error) {
        console.log(error);
      }
    };
  }
};

export default SearchThunk;