import DetailsAction from '../actions/DetailsAction';
import { ProductDetails } from './../../services/ProductDetails';

const DetailsThunk = {
  getProductInfo: id => {
    return async dispatch => {

      try {
        const response = await ProductDetails.getInfo(`https://api.mercadolibre.com/items/${id}`);
        dispatch(DetailsAction.getInfo(response.data));
      }

      catch (error) {
        console.log(error);
      }
    };
  },
  getDescription: id => {
    return async dispatch => {

      try {
        const response = await ProductDetails.getInfo(`https://api.mercadolibre.com/items/${id}/description`);
        dispatch(DetailsAction.getDescription(response.data.plain_text));
      }

      catch (error) {
        console.log(error);
      }
    };
  }
};

export default DetailsThunk;