import {
  GET_PRODUCT_INFO,
  GET_PRODUCT_DESCRIPTION
} from '../constants';

const DetailsAction = {
  getInfo: data => ({
    payload: data,
    type: GET_PRODUCT_INFO
  }),
  getDescription: data => ({
    payload: data,
    type: GET_PRODUCT_DESCRIPTION
  })
};

export default DetailsAction;