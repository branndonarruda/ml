import { GET_PRODUCTS } from '../constants';

const SearchAction = {
  search: data => ({
    payload: data,
    type: GET_PRODUCTS
  })
};

export default SearchAction;