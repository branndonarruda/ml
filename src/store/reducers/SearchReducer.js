import { GET_PRODUCTS } from '../constants';

const initialState = {
  results: []
};

const SearchReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCTS:
      return {
        ...state,
        results: action.payload
      };
    default:
      return state;
  }
};

export default SearchReducer;