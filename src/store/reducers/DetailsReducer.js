import {
  GET_PRODUCT_INFO,
  GET_PRODUCT_DESCRIPTION
} from '../constants';

const initialState = {
  product: [],
  description: ''
};

const DetailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCT_INFO:
      return {
        ...state,
        product: action.payload
      };
    case GET_PRODUCT_DESCRIPTION:
      return {
        ...state,
        description: action.payload
      };
    default:
      return state;
  }
};

export default DetailsReducer;