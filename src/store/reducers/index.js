
import { combineReducers } from 'redux';

// Reducers
import SearchReducer from './SearchReducer';
import DetailsReducer from './DetailsReducer';

export const Reducers = combineReducers({
  search: SearchReducer,
  details: DetailsReducer
});