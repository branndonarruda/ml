const express = require("express");
const router = express.Router();
const searchController = require("../controllers/searchController");

router.get("/:query", searchController.search);
router.get("/details/:query", searchController.details);
module.exports = router;
