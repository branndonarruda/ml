const express = require("express");
const app = express();
const router = express.Router();

// Routes
const index = require("./routes/index");
const searchRoute = require("./routes/searchRoute");

app.use("/", index);
app.use("/search", searchRoute);
module.exports = app;
