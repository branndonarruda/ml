const axios = require("axios");

/**
 * search endpoint to return results of query search
 */
exports.search = (req, res, next) => {
  let query = req.params.query;
  axios.get(`https://api.mercadolibre.com/sites/MLA/search?q=${query}`).then(function(response) {
    const result = {
      author = {
        name: response.data.site_id,
        lastname: response.data.query
      },
      categories: '',
      items: [ response.data.results ]
    };
    res.status(response.status).send(result);
  });
};

/**
 * details endpoint to return more details of product selected
 */
exports.details = (req, res, next) => {
  let query = req.params.query;
  axios.get(`https://api.mercadolibre.com/items/${query}/description`).then(function(response) {
    const result = {
      author = {
        name: "",
        lastname: ""
      },
      item = {
        id: "",
        title: "",
        price = {
          currency: "",
          amount: null,
          decimals: null,
        },
        picture: "",
        condition: "",
        free_shipping: false,
        sold_quantity: null,
        description: ""
      }
    };
    res.status(response.status).send(result);
  });
};
