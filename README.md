# Mercado Livre
Teste Front-End

### Pre requisitos
- Yarn ou NPM
- Node

### Tecnologias utilizadas
- React + Redux
- Redux-Thunk (middleware)
- SCSS - BEM CSS
- Testes de snapshot com Enzyme

## 1 - Baixe o Projeto
Faça o Download do Projeto ou clone ele
```cmd
git clone https://bitbucket.org/branndonarruda/ml/src/develop/
```

## 2 - Instale as Dependências
depois de baixar/clonar, entre dentro da pasta do projeto, e instale as dependências com o seguinte comando:
```cmd
yarn install
```
## 3 - Rode o Projeto
Para dar build no projeto e visualizar a webapp em seu browser, rode o seguinte comando:
```cmd
yarn start
```
A webapp irá executar na porta 3000 do seu navegador.

## 4 - Finalizado
Para acessar a aplicação, acesse o endereço abaixo no seu navegador:
[http://localhost:3000/](http://localhost:3000/)

## 5 - Gerando o DIST para produção
Para gerar o DIST minificado para produção, em seu terminal rode o seguinte comando:
```cmd
yarn build
```